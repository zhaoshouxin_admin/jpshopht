import goodsData from './data/goods'

export default [
  {
    url: '/merchantcategory',
    type: 'get',
    response: () => {
      return {
        status:200,
        msg:"请求成功",
        count: 24,
        data: goodsData.goodsGroup
      }
    }
  },
  // {
  //   url: '/merchantcategory',
  //   type: 'post',
  //   response: () => {
  //     return {
  //       status: 200,
  //       message: '请求成功'
  //     }
  //   }
  // },
  // {
  //   url: '/merchantcategory/*',
  //   type: 'put',
  //   response: () => {
  //     return {
  //       status: 200,
  //       message: '修改成功'
  //     }
  //   }
  // },
  // {
  //   url: '/merchantcategory/*',
  //   type: 'DELETE',
  //   response: () => {
  //     return {
  //       status: 200,
  //       message: '删除成功'
  //     }
  //   }
  // },


  {
    url: '/merchantgoodscitygroup',
    type: 'get',
    response: () => {
      return goodsData.goodArea
    }
  },
  {
    url: '/merchantgoodscitygroup',
    type: 'post',
    response: () => {
      return {
        status: 200,
        message: '添加成功！'
      }
    }
  },

  {
    url: '/addr',
    type: 'get',
    response: () => {
      return goodsData.addr
    }
  },
  
]